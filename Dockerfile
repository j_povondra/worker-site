FROM python:3.10-slim

RUN useradd wagtail

ENV PYTHONUNBUFFERED=1 \
    PORT=8000

COPY requirements.txt requirements.txt

RUN apt update && apt -y install git gcc default-libmysqlclient-dev build-essential

RUN pip install --no-cache-dir -r requirements.txt

RUN pip install "gunicorn"

WORKDIR /app

RUN chown wagtail:wagtail /app

COPY --chown=wagtail:wagtail . .

USER wagtail

ENV PATH="/usr/bin:$PATH"

RUN chmod 755 scripts/run_server.sh

EXPOSE 8000
