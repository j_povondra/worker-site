from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.admin.panels import (
    FieldPanel, FieldRowPanel,
    InlinePanel, MultiFieldPanel
)
from wagtail.fields import RichTextField
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField

from wagtail.contrib.forms.forms import FormBuilder

from django.core.mail import send_mail
from django.conf import settings

class CustomFormBuilder(FormBuilder):

    def get_create_field_function(self, type):
        create_field_function = super().get_create_field_function(type)
        def wrapped_create_field_function(field, options):
            created_field = create_field_function(field, options)
            created_field.widget.attrs.update(
                {
                    "class": f'form-control {field.field_class}',
                    "placeholder": field.placeholder,
                }
            )

            return created_field

        return wrapped_create_field_function

class FormField(AbstractFormField):
    page = ParentalKey('KontaktPage', on_delete=models.CASCADE, related_name='form_fields')

    field_class = models.CharField("CSS třída", max_length=254, blank=True)
    placeholder = models.CharField("Placeholder", max_length=254, blank=True)

    panels = AbstractFormField.panels + [
        MultiFieldPanel([
            FieldPanel("field_class"),
            FieldPanel("placeholder"),
        ], heading="Pro vývojáře"),
    ]


class KontaktPage(AbstractEmailForm):
    form_builder = CustomFormBuilder

    uvodni_text = RichTextField(blank=True)

    tel_cislo= models.CharField(max_length=50,null=True,blank=True)

    email=models.CharField(max_length=150,null=True,blank=True)

    address=models.CharField(max_length=250,null=True,blank=True)

    dekujeme_text = RichTextField(blank=True)

    bod_na_mape = models.CharField(max_length=500, blank=False, null=True)

    template = "kontakt/kontakt_page.html"

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel("show_in_menus"),
        MultiFieldPanel([
            FieldPanel('tel_cislo'),
            FieldPanel('email'),
            FieldPanel('address'),
    ],heading="Kontaktní údaje"),
        FieldPanel('bod_na_mape'),
        FieldPanel('uvodni_text'),
        InlinePanel('form_fields', label="Form fields"),
        FieldPanel('dekujeme_text'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    def send_mail(self, form):

        res= send_mail(subject="testovaci mail", message=self.render_email(form), from_email=settings.EMAIL_HOST_USER, recipient_list=['jakubpovondra@gmail.com',])
        print("##############")
        print(res)
