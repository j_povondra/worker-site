# Generated by Django 4.1.7 on 2023-03-24 14:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kontakt', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='formfield',
            name='field_class',
            field=models.CharField(blank=True, max_length=254, verbose_name='CSS třída'),
        ),
        migrations.AddField(
            model_name='formfield',
            name='placeholder',
            field=models.CharField(blank=True, max_length=254, verbose_name='Placeholder'),
        ),
    ]
