from django.db import models
from django import forms
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django_extensions.db.fields import AutoSlugField


from modelcluster.fields import ParentalManyToManyField

from streams import blocks

from wagtail_photo_gallery.models import ImageGalleryMixin
from wagtail.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.models import register_snippet


class Stavba(ImageGalleryMixin, Page):
    template = "stavby/detail_stavby.html"

    uvodni_obrazek = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        related_name="+",
        on_delete=models.SET_NULL
    )

    nazev = models.CharField(blank=False, null=True, max_length=200)

    nadpis = models.CharField(blank=True, null=True, max_length=200)

    kategorie = ParentalManyToManyField("stavby.Kategorie", blank=True)

    autori = ParentalManyToManyField("stavby.Autor", blank=True)

    datum = models.DateTimeField(auto_now_add=True, null=True)

    popis = RichTextField(blank=True, features=['bold', 'italic', 'link'])

    obsah = StreamField(
        [
            ("gallery", blocks.MyGalleryBlock()),
            ("odstavec", blocks.ZvyraznenyOdstavec()),
            ("textovy_editor", blocks.LimitedRichtextBlock()),
        ],
        blank=True,
        null=True,
    )

    content_panels = Page.content_panels + [
        ImageChooserPanel('uvodni_obrazek'),
        MultiFieldPanel([
            FieldPanel('nazev'),
            FieldPanel('nadpis'),
            FieldPanel('popis'),
        ], heading="Název a popis stavby"),
        StreamFieldPanel('obsah'),
        MultiFieldPanel(
            [
                FieldPanel("kategorie", widget=forms.CheckboxSelectMultiple),
            ],
            heading="Kategorie"
        ),
        MultiFieldPanel(
            [
                FieldPanel("autori", widget=forms.CheckboxSelectMultiple),
            ],
            heading="Autoři"
        ),

    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        parent = SouhrnStaveb.objects.live().public().first()
        ostatni_stavby = Stavba.objects.live().public().all().exclude(nazev=self.nazev).order_by('id')[:3]
        vsechny_karegorie = Kategorie.objects.all()

        context['parent'] = parent
        context['ostatni_stavby'] = ostatni_stavby
        context['vsechny_karegorie'] = vsechny_karegorie
        return context


class SouhrnStaveb(Page):
    max_num = 1
    template = "stavby/souhrn_staveb.html"

    vlastni_titulek = models.CharField(
        max_length=255,
        blank=True,
        null=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("show_in_menus"),
        FieldPanel("vlastni_titulek"),
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        seznam_kategorii = Kategorie.objects.all()[:5]
        kategorie = request.GET.get('c', False)
        autor = request.GET.get('autor', False)

        if kategorie:
            stavby = Stavba.objects.filter(kategorie__slug=kategorie).live().public()
        elif autor:
            stavby = Stavba.objects.filter(autori__slug=autor).live().public()
        else:
            stavby = Stavba.objects.live().public()

        paginator = Paginator(stavby, 3)
        strana = request.GET.get("strana")

        try:
            stavby = paginator.page(strana)
        except PageNotAnInteger:
            stavby = paginator.page(1)
        except EmptyPage:
            stavby = paginator.page(paginator.num_pages)

        context['kategorie'] = seznam_kategorii
        context['filtr'] = kategorie
        context["stavby"] = stavby

        return context

    class Meta:
        verbose_name = "Souhrn staveb"


@register_snippet
class Kategorie(models.Model):
    nazev = models.CharField(max_length=255, blank=False, null=True)
    slug = AutoSlugField(populate_from="nazev", editable=True)

    panels = [
        MultiFieldPanel([
            FieldPanel("nazev"),
        ],
            heading="Kategorie"),
    ]

    class Meta:
        verbose_name = "Kategorie"
        verbose_name_plural = "Kategorie staveb"

    def __str__(self):
        return self.nazev


@register_snippet
class Autor(models.Model):
    nazev = models.CharField(max_length=255, blank=False, null=True)
    slug = AutoSlugField(populate_from="nazev", editable=True)

    panels = [
        MultiFieldPanel([
            FieldPanel("nazev"),
        ],
            heading="Kategorie"),
    ]

    class Meta:
        verbose_name = "Autor"
        verbose_name_plural = "Autoři"

    def __str__(self):
        return self.nazev
