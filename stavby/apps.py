from django.apps import AppConfig


class StavbyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'stavby'
