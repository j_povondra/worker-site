#!/bin/sh

python manage.py collectstatic --noinput
python manage.py migrate

gunicorn AlesMates.wsgi --preload --workers=${1:-2} --threads=${2:-4} --worker-class=${3:-gthread} --timeout 300 -b 0.0.0.0:8000 --log-level=info
