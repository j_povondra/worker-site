from django.db import models

from streams import blocks
from wagtail.models import Page

from wagtail.admin.panels import StreamFieldPanel, MultiFieldPanel, FieldPanel
from wagtail.fields import StreamField

from recenze.models import RecenzeFormular


class HomePage(Page):
    template = "home/index.html"
    max_count = 1

    KartyHlavniStrana = StreamField([
        ("KartySekce", blocks.KartyHlavniStranaBlock())
    ],
        null=True,
        blank=False,
    )

    SluzbyHlavniStrana = StreamField([
        ("SluzbySekce", blocks.SluzbyHlavniStranaBlock()),
    ],
        null=True,
        blank=False,
    )

    PocitadlaHlavniStrana = StreamField([
        ("CounterSekce", blocks.CounterHlavniStranaBlock()),
    ],
        null=True,
        blank=False,
    )

    CarouselHlavniStrana = StreamField([
        ("Carousel", blocks.CarouselHlavniStranaBlock()),
    ],
        null=True,
        blank=False,
    )

    content_panels = Page.content_panels + [
        FieldPanel("show_in_menus"),
        MultiFieldPanel(
            [
                StreamFieldPanel("CarouselHlavniStrana"),
            ],
            heading="Carousel hlavní strana",
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel("KartyHlavniStrana"),
            ],
            heading="Karty hlavní strana",
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel("SluzbyHlavniStrana"),
            ],
            heading="Služby hlavní strana",
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel("PocitadlaHlavniStrana"),
            ],
            heading="Počítadla",
        ),

    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["RecenzeFormular"] = RecenzeFormular()

        return context

    def serve(self, request):
        if request.method == "POST":
            form = RecenzeFormular(request.POST)
            if form.is_valid():
                form.save()
        return super().serve(request)
