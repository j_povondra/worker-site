from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock

# Přepis funkce pro vyrendrování správného templatu
from wagtail_photo_gallery.models import GalleryBlock
from wagtail_photo_gallery.models import ImageGalleryMixin
from wagtail.contrib.routable_page.models import route
from django.shortcuts import render


@route(r'^album/(.+)/$')
def serve_album(self, request, slug):
    for gallery in self._gallery_blocks:
        try:
            album = gallery.block.filter_albums(gallery.value).get(slug=slug)
        except Album.DoesNotExist:
            continue
        return render(
            request,
            'streams/album_detail.html',
            {'page': self, 'album': album, 'images': album.images.all()}
        )
    raise Http404


ImageGalleryMixin.serve_album = serve_album


class SluzbyHlavniStranaBlock(blocks.StructBlock):
    titul = blocks.CharBlock(required=False)

    karty = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("titul", blocks.CharBlock(required=True, max_length=50)),
                ("text", blocks.TextBlock(required=False, max_length=120)),
                ("ikona", blocks.CharBlock(required=False, max_length=30)),
            ]
        )
    )

    text = blocks.RichTextBlock(required=False)

    btnOdkaz = blocks.PageChooserBlock(required=False)
    btnText = blocks.CharBlock(required=False)

    class Meta:
        template = "streams/sluzby_main_block.html"


class KartyHlavniStranaBlock(blocks.StructBlock):
    karty = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("titul", blocks.CharBlock(required=True, max_length=50)),
                ("text", blocks.TextBlock(required=False, max_length=120)),
                ("ikona", blocks.CharBlock(required=False, max_length=30)),
            ]
        )
    )

    class Meta:
        template = "streams/karty_main_block.html"


class CounterHlavniStranaBlock(blocks.StructBlock):
    pocitadla = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("text", blocks.CharBlock(required=False, max_length=50)),
                ("cislo", blocks.CharBlock(required=False, max_length=20)),
            ]
        )
    )

    class Meta:
        template = "streams/counter_main_block.html"


class CarouselHlavniStranaBlock(blocks.StructBlock):
    slides = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("image", ImageChooserBlock(required=False)),
                ("nazevFirmy", blocks.CharBlock(max_length=50,required=False)),
                ("nadpisPrvni", blocks.CharBlock(max_length=400)),
                ("nadpisDruhy", blocks.CharBlock(max_length=400)),
                ("text", blocks.CharBlock(max_length=500, required=False)),
                ("textTlacitka", blocks.CharBlock(max_length=80, required=True)),
                ("odkazTlacitka", blocks.PageChooserBlock(required=True)),
            ]
        )
    )

    class Meta:
        icon = 'image'
        template = "streams/carousel_main_block.html"
        label = "Přehlídka obrázků"


class MyGalleryBlock(GalleryBlock):
    class Meta:
        template = "streams/gallery.html"
        icon = 'image'
        label = "Galerie"


class ZvyraznenyOdstavec(blocks.StructBlock):
    text = blocks.CharBlock(max_length=400)

    class Meta:
        template = "streams/zvyrazneny_odstavec.html"
        label = "Zvýrazněný odstavec"


class LimitedRichtextBlock(blocks.RichTextBlock):
    def __init__(
            self,
            required=True,
            help_text=None,
            editor="default",
            features=None,
            max_length=None,
            validators=(),
            **kwargs,
    ):
        super().__init__(**kwargs)
        self.features = [
            'bold',
            'italic',
            'link',
            'ol',
            'ul',
            'hr',
            'h3',
            'h4',
            'h5',
            'h6',
            'image',
            'embed',


        ]

    class Meta:
        template = "streams/richtext_block.html"
        icon = "edit"
        label = "Textový Editor"


class RichtextBlock(blocks.RichTextBlock):
    class Meta:
        template = "streams/richtext_block.html"
        icon = "edit"
        label = "Textový Editor"
