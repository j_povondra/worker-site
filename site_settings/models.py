from django.db import models
from wagtail.admin.panels import FieldPanel, MultiFieldPanel
from wagtail.contrib.settings.models import BaseSetting,register_setting




@register_setting
class ObecneInformaceNastaveni(BaseSetting):
    tel_cislo = models.CharField(max_length=50, blank=True, null=True)

    email = models.CharField(max_length=150, blank=True, null=True)

    nazev_stranky = models.CharField(max_length=250, blank=False, null=True)

    adresa = models.CharField(max_length=500, blank=True, null=True)

    popis_footer = models.TextField(blank=True, null=True)

    panels = [
        MultiFieldPanel([
            FieldPanel("tel_cislo"),
            FieldPanel("email"),
            FieldPanel("adresa"),
        ], heading="Obecné kontaktní informace"),
        MultiFieldPanel([
            FieldPanel("nazev_stranky"),
            FieldPanel("popis_footer"),
        ], heading="Globální nastavení"),
    ]
