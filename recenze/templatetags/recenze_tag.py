from django import template
from ..models import Recenze

register = template.Library()


@register.inclusion_tag('recenze/recenze.html', takes_context=True)
def RecenzeTag(context):
    return {
        'vsechny_recenze': Recenze.objects.all(),
        'request': context['request'],
    }
