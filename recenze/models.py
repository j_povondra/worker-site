from django.db import models

from wagtail.admin.panels import FieldPanel
from wagtail.snippets.models import register_snippet

# from wagtail.core.models import Page

from django.forms import ModelForm


@register_snippet
class Recenze(models.Model):
    jmeno = models.CharField(null=True, blank=True, max_length=100)
    text = models.TextField(blank=False, null=True)
    hodnoceni = models.IntegerField(null=True, blank=False)
    platnost = models.BooleanField(default=False)

    panels = [
        FieldPanel('jmeno'),
        FieldPanel('text'),
        FieldPanel('hodnoceni'),
        FieldPanel('platnost'),
    ]

    def __str__(self):
        return self.jmeno

    class Meta:
        verbose_name_plural = 'Recenze'


# class SouhrnRecenzi(Page):
#     max_num = 0
#
#     template = "recenze/souhrn.html"
#
#     nadpis = models.CharField(
#         max_length=255,
#         blank=True,
#         null=True,
#     )
#
#     content_panels = Page.content_panels + [
#         FieldPanel("nadpis"),
#     ]
#
#     def get_context(self, request, *args, **kwargs):
#         context = super().get_context(request, *args, **kwargs)
#         context["RecenzeFormular"] = RecenzeFormular()
#
#         return context
#
#     def serve(self, request):
#         if request.method == "POST":
#             form = RecenzeFormular(request.POST)
#             if form.is_valid():
#                 form.save()
#         return super().serve(request)


class RecenzeFormular(ModelForm):
    class Meta:
        model = Recenze
        fields = ('jmeno', "text", "hodnoceni")

        labels = {
            "jmeno": "Vaše celé jméno",
            "text": "Text recenze",
            "hodnoceni": ""
        }

    def __init__(self, *args, **kwargs):
        super(RecenzeFormular, self).__init__(*args, **kwargs)
        self.fields['hodnoceni'].widget.attrs.update({
            'id': 'pocetHvezdicek',
            'style': 'display:none;',
        })
        self.fields['jmeno'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['text'].widget.attrs.update({
            'class': 'form-control',
        })
